#!/bin/bash
set -e # exit if any command fails

if [[ -z $1 ]]; then
  echo "Usage: $0 flash_config_file.flashconfig"
  exit 1
fi

source "$1"

# create temporary config file (to be filled with sensitive passwords)
ESPHOME_YAML_FILE_TMP="$ESPHOME_YAML_FILE.tmp"
cp "$ESPHOME_YAML_FILE" "$ESPHOME_YAML_FILE_TMP"

# replace password placeholders with values from flash_config file
replace 'WIFI_SSID'        "$WIFI_SSID"        -- $ESPHOME_YAML_FILE_TMP
replace 'WIFI_PASSWORD'    "$WIFI_PASSWORD"    -- $ESPHOME_YAML_FILE_TMP
replace 'HOTSPOT_SSID'     "$HOTSPOT_SSID"     -- $ESPHOME_YAML_FILE_TMP
replace 'HOTSPOT_PASSWORD' "$HOTSPOT_PASSWORD" -- $ESPHOME_YAML_FILE_TMP
replace 'OTA_PASSWORD'     "$OTA_PASSWORD"     -- $ESPHOME_YAML_FILE_TMP

# upload config to esp
esphome "$ESPHOME_YAML_FILE_TMP" run --upload-port "$ESP_HOST"

# upload via usb interface
#sudo chmod a+rw /dev/ttyUSB0
#esphome "$ESPHOME_YAML_FILE_TMP" upload --upload-port /dev/ttyUSB0

# remove temporary flash_config file
rm "$ESPHOME_YAML_FILE_TMP"
